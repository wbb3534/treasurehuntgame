package com.seop.treasurehuntgame;

import com.fasterxml.jackson.databind.JsonSerializable;
import com.seop.treasurehuntgame.entity.Treasure;
import com.seop.treasurehuntgame.model.ChooseTreasureItem;
import com.seop.treasurehuntgame.repository.TreasureRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;
import java.util.LinkedList;
import java.util.List;

@SpringBootTest
class TreasureHuntGameApplicationTests {
    @Autowired //main에 있는 정보를 가져올때 사용 자동으로 이어주겟다는 뜻
    private TreasureRepository treasureRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void testPercent() {
        List<Treasure> originList = treasureRepository.findAll();

        List<ChooseTreasureItem> result = new LinkedList<>();

        double minPercent = 0D; //id = 0 번에 최소값이 0이기 때문
        for (Treasure item : originList) {
            ChooseTreasureItem addItem = new ChooseTreasureItem(); // 리스트에 담을 그릇

            addItem.setId(item.getId());        // 아이디를 가져옴
            addItem.setMinPercent(minPercent);  // 최소값은 minPercent이며  += item.getGeneralProbability() 할에정
            addItem.setMaxPercent(minPercent + item.getGeneralProbability()); // 최대값은 최소값 + item.getGeneralProbability()
            minPercent += item.getGeneralProbability(); // 최소값은 최소값 + GeneralProbability()이기때문에

            result.add(addItem); // result에 addItemd을 넣어준다
        }

        double percentResult = Math.random() * 100; //최대 100까지 랜덤숫자가 나온다

        long resultId = 0; //조건에 해당하는 id를 알아내기위해 기본값을 0으로 둠
        for (ChooseTreasureItem item : result) { //for문을 통해 result를 하나씩 던져 주면서
            // 만약 랜덤으로 생성된 숫자가 아이템에 최소값보다 크거나 같거나 랜덤으로 생성된 숫자가 아이템 최대값보다 작으면
            // resultId에다가 조건문에 해당되는 아이템 아이디를 넣는다
            // 포문을 다도는것이 아니라 만약 랜덤으로 생성되는 숫자가 일치하면 멈추기 위해 break; 를 사용
            if (percentResult >= item.getMinPercent() && percentResult <= item.getMaxPercent()) {
                resultId = item.getId();
                break;
            }
        }
        System.out.println("생성된 랜덤 숫자 :" + percentResult);
        System.out.println("해당하는 보물 id :" + resultId);
    }
}

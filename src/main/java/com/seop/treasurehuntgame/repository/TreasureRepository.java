package com.seop.treasurehuntgame.repository;

import com.seop.treasurehuntgame.entity.Treasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TreasureRepository extends JpaRepository<Treasure, Long> {
}

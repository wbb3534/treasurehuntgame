package com.seop.treasurehuntgame.repository;

import com.seop.treasurehuntgame.entity.Gold;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoldRepository extends JpaRepository<Gold, Long> {
}

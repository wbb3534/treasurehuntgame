package com.seop.treasurehuntgame.repository;

import com.seop.treasurehuntgame.entity.HaveTreasure;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface HaveTreasureRepository extends JpaRepository<HaveTreasure, Long> {
    Optional<HaveTreasure> findByTreasure_Id(long treasureId);
    List<HaveTreasure> findAllByIdGreaterThanEqualOrderByIdAsc(long id); // GreaterThanEqual 1보다 크면 가져온다
}

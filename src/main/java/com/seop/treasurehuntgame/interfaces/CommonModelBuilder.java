package com.seop.treasurehuntgame.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

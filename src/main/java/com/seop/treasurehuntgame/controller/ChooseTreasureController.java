package com.seop.treasurehuntgame.controller;

import com.seop.treasurehuntgame.model.ChooseTreasureResponse;
import com.seop.treasurehuntgame.model.ListResult;
import com.seop.treasurehuntgame.model.SingleResult;
import com.seop.treasurehuntgame.model.UseTreasureItem;
import com.seop.treasurehuntgame.service.ChooseTreasureService;
import com.seop.treasurehuntgame.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "보물 뽑기")
@RestController
@RequestMapping("/v1/choose-treasure")
@RequiredArgsConstructor
public class ChooseTreasureController {
    private final ChooseTreasureService chooseTreasureService;

    @ApiOperation(value = "일반뽑기")
    @PostMapping("/general")
    public SingleResult<ChooseTreasureResponse> chooseGeneral() {
        return ResponseService.getSingleResult(chooseTreasureService.getResult(true));
    }

    @ApiOperation(value = "고급뽑기")
    @PostMapping("/high-quality")
    public SingleResult<ChooseTreasureResponse> chooseHighQuality() {
        return ResponseService.getSingleResult(chooseTreasureService.getResult(false));
    }

    @ApiOperation(value = "내가보유한 보물 리스트")
    @GetMapping("/list")
    public List<UseTreasureItem> getTreasures() {
        return chooseTreasureService.getMyTreasure();
    }
}

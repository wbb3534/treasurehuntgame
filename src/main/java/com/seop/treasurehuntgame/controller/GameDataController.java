package com.seop.treasurehuntgame.controller;

import com.seop.treasurehuntgame.model.CommonResult;
import com.seop.treasurehuntgame.model.GameDataResponse;
import com.seop.treasurehuntgame.model.SingleResult;
import com.seop.treasurehuntgame.service.GameDataService;
import com.seop.treasurehuntgame.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "게임데이터 관리")
@RestController
@RequestMapping("/v1/game-data")
@RequiredArgsConstructor
public class GameDataController {
    private final GameDataService gameDataService;

    @ApiOperation(value = "처음접속시 데이터")
    @GetMapping("/init")
    public SingleResult<GameDataResponse> getFirstData() {
        return ResponseService.getSingleResult(gameDataService.getFirstData());
    }
    @ApiOperation(value = "게임 초기화")
    @DeleteMapping("/reset")
    public CommonResult resetGame() {
        gameDataService.gameReset();
        return ResponseService.getSuccessResult();
    }
}

package com.seop.treasurehuntgame.controller;

import com.seop.treasurehuntgame.model.AddGoldResponse;
import com.seop.treasurehuntgame.model.SingleResult;
import com.seop.treasurehuntgame.service.GoldService;
import com.seop.treasurehuntgame.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "골드 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/gold")
public class GoldController {
    private final GoldService goldService;

    @ApiOperation(value = "클릭시 골드 추가")
    @PutMapping("/add")
    public SingleResult<AddGoldResponse> putAddGold() {
        return ResponseService.getSingleResult(goldService.putAddGold());
    }
}

package com.seop.treasurehuntgame.advice;

import com.seop.treasurehuntgame.enums.ResultCode;
import com.seop.treasurehuntgame.exception.CMissingDataException;
import com.seop.treasurehuntgame.exception.CNonGoldException;
import com.seop.treasurehuntgame.exception.CWrongPhoneNumberException;
import com.seop.treasurehuntgame.model.CommonResult;
import com.seop.treasurehuntgame.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CNonGoldException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNonGoldException e) {
        return ResponseService.getFailResult(ResultCode.NON_GOLD);
    }

}


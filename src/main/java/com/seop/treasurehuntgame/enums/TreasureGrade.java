package com.seop.treasurehuntgame.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TreasureGrade {
    RARE("레어")
    , EPIC("에픽")
    , UNIQUE("유니크")
    , MYTH("신화")

    ;
    private final String name;
}

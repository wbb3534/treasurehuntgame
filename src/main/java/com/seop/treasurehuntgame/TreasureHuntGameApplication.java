package com.seop.treasurehuntgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreasureHuntGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(TreasureHuntGameApplication.class, args);
    }

}

package com.seop.treasurehuntgame.service;

import com.seop.treasurehuntgame.entity.Gold;
import com.seop.treasurehuntgame.entity.HaveTreasure;
import com.seop.treasurehuntgame.entity.Treasure;
import com.seop.treasurehuntgame.enums.TreasureGrade;
import com.seop.treasurehuntgame.model.TreasureRequest;
import com.seop.treasurehuntgame.repository.GoldRepository;
import com.seop.treasurehuntgame.repository.HaveTreasureRepository;
import com.seop.treasurehuntgame.repository.TreasureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class InitDataService {
    private final GoldRepository goldRepository;
    private final HaveTreasureRepository haveTreasureRepository;
    private final TreasureRepository treasureRepository;

    public void setFirstHaveGold() {
        //goldRepository에 Id가 1인애가 있거나 없거나 가져온다
        Optional<Gold> originData = goldRepository.findById(1L);

        // 만약 originData가 비어있으면
        if (originData.isEmpty()) {
            //GoldBuilder()을 실행한다 haveGold를 0이라고한다
            Gold addData = new Gold.GoldBuilder().build();
            //goldRepository에 addData를 저장
            goldRepository.save(addData);
        }
    }
    public void setTreasureInfo() {
        //리스트로 treasureRepository를 다찾아온다.
        List<Treasure> originData = treasureRepository.findAll();
        // 만약 originData의 크기가 0이면 디비에 아무것도 없다는 뜻
        if (originData.size() == 0) {
            // List<TreasureRequest>를 담을 그릇을 만든다
            List<TreasureRequest> result = new LinkedList<>();
            // TreasureRequestBuilder를 이용하여 보물 정보를 담는다.
            TreasureRequest request1 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.RARE, "금동 관음보살", "gilt_bodhisattva.png", 10D, 5.1).build();
            // 정보를 담고 result에 저장
            result.add(request1);

            TreasureRequest request2 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.RARE, "금 목걸이", "gold_necklace.png", 10D, 5.1).build();
            result.add(request2);

            TreasureRequest request3 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.RARE, "내합", "by_laws.png", 10D, 5.1).build();
            result.add(request3);

            TreasureRequest request4 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.RARE, "농경문", "agricultural _gate.png", 10D, 5.1).build();
            result.add(request4);

            TreasureRequest request5 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.RARE, "빅완구", "big_toy.png", 10D, 5.1).build();
            result.add(request5);

            TreasureRequest request6 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.RARE, "드리개", "drill_it.png", 10D, 5.1).build();
            result.add(request6);

            TreasureRequest request7 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.EPIC, "띠고리", "band_ring.png", 5D, 8D).build();
            result.add(request7);

            TreasureRequest request8 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.EPIC, "분청사기", "buncheong_ware.png", 5D, 8D).build();
            result.add(request8);

            TreasureRequest request9 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.EPIC, "신라 귀걸이", "sinra_earrings.png", 5D, 8D).build();
            result.add(request9);

            TreasureRequest request10 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.EPIC, "신라의 미소", "sinra_smile.png", 5D, 8D).build();
            result.add(request10);

            TreasureRequest request11 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.EPIC, "동종", "bronze_bell.png", 5D, 8D).build();
            result.add(request11);

            TreasureRequest request12 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.UNIQUE, "청년 방울", "youth_drop.png", 3D, 5.1).build();
            result.add(request12);

            TreasureRequest request13 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.UNIQUE, "화자 총통", "generalissimo.png", 3D, 5.1).build();
            result.add(request13);

            TreasureRequest request14 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.UNIQUE, "고려청자", "goryeo_celadon.png", 3D, 5.1).build();
            result.add(request14);

            TreasureRequest request15 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.UNIQUE, "논갈이", "plowing_rice_fields.jpeg", 3D, 5.1).build();
            result.add(request15);

            TreasureRequest request16 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.MYTH, "다이아몬드", "diamond.png", 1D, 3D).build();
            result.add(request16);

            TreasureRequest request17 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.MYTH, "태극기", "taegeukgi.jpeg", 1D, 3D).build();
            result.add(request17);

            TreasureRequest request18 = new TreasureRequest.TreasureRequestBuilder(TreasureGrade.MYTH, "보물지도", "treasure_map.jpeg", 1D, 3D).build();
            result.add(request18);

            // for문을 이용하여 보물정보가 들어있는 item을 TreasureBuilder에게 전달 해준다.
            result.forEach(item -> {
                Treasure addData = new Treasure.TreasureBuilder(item).build();
                // treasureRepository에 addData 저장
                treasureRepository.save(addData);
            });
        }

    }
    public void setHaveCount() {
        // 보유한 보물을 리스트로 뽑아온다
        List<HaveTreasure> haveTreasuresList = haveTreasureRepository.findAll();

        // 만약 보유한 보물의 데이터가 하나도 없으면 ....
        if (haveTreasuresList.size() == 0) {
            // 보물 리스트를 뽑아온다 .. 보물 id를 얻기 위함
            List<Treasure> treasureList = treasureRepository.findAll();

            //for문을 통해서 HaveTreasureBuilder에 item을 던진다
            treasureList.forEach(item -> {
                HaveTreasure addData = new HaveTreasure.HaveTreasureBuilder(item).build();
                //haveTreasureRepository에 addData를 저장
                haveTreasureRepository.save(addData);
            });
            /*for (Treasure item : treasureList) {
                HaveTreasure addData = new HaveTreasure.HaveTreasureBuilder(item).build();
                haveTreasureRepository.save(addData);
            }*/
        }
    }

}

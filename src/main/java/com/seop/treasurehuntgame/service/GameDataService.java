package com.seop.treasurehuntgame.service;

import com.seop.treasurehuntgame.entity.Gold;
import com.seop.treasurehuntgame.entity.HaveTreasure;
import com.seop.treasurehuntgame.exception.CMissingDataException;
import com.seop.treasurehuntgame.model.AddGoldResponse;
import com.seop.treasurehuntgame.model.GameDataResponse;
import com.seop.treasurehuntgame.model.UseTreasureItem;
import com.seop.treasurehuntgame.repository.GoldRepository;
import com.seop.treasurehuntgame.repository.HaveTreasureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GameDataService {
    private final GoldRepository goldRepository;
    private final HaveTreasureRepository haveTreasureRepository;

    /**
     * 게임 접속시 처음 데이터
     * @return
     */
    public GameDataResponse getFirstData() {
        GameDataResponse result = new GameDataResponse();

        Gold gold = goldRepository.findById(1L).orElseThrow(CMissingDataException::new);

        result.setAddGoldResponse(new AddGoldResponse.AddGoldResponseBuilder(gold).build());
        result.setUseTreasureItem(this.getMyTreasure());

        return result;
    }
    public void gameReset() {
        Gold gold = goldRepository.findById(1L).orElseThrow(CMissingDataException::new);

        gold.resetGold();

        goldRepository.save(gold);

        List<HaveTreasure> originList = haveTreasureRepository.findAll();

        for (HaveTreasure item : originList) {
            item.resetCount();

            haveTreasureRepository.save(item);
        }
    }
    /**
     * 내가 보유한 보물들을 불러온다
     * @return 보유한 보물 컬렉션
     */
    public List<UseTreasureItem> getMyTreasure() {
        List<HaveTreasure> originList = haveTreasureRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L);

        List<UseTreasureItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new UseTreasureItem.UseTreasureItemBuilder(item).build()));

        return result;
    }
}

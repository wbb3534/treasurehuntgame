package com.seop.treasurehuntgame.service;

import com.seop.treasurehuntgame.entity.Gold;
import com.seop.treasurehuntgame.exception.CMissingDataException;
import com.seop.treasurehuntgame.model.AddGoldResponse;
import com.seop.treasurehuntgame.repository.GoldRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GoldService {
    private final GoldRepository goldRepository;

    public AddGoldResponse putAddGold() {
        // goldRepository에서 id가 1인 애를 찾아오고 없으면 CMissingDataException로 탈출
        Gold gold = goldRepository.findById(1L).orElseThrow(CMissingDataException::new);
        // putAddGold를 실행
        gold.putAddGold();
        // goldRepository에 저장
        Gold result = goldRepository.save(gold);
        // AddGoldResponse를 통해 리턴값을 나타냄
        return new AddGoldResponse.AddGoldResponseBuilder(result).build();
    }
}

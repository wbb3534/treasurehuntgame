package com.seop.treasurehuntgame.service;

import com.seop.treasurehuntgame.entity.Gold;
import com.seop.treasurehuntgame.entity.HaveTreasure;
import com.seop.treasurehuntgame.entity.Treasure;
import com.seop.treasurehuntgame.exception.CMissingDataException;
import com.seop.treasurehuntgame.exception.CNonGoldException;
import com.seop.treasurehuntgame.model.*;
import com.seop.treasurehuntgame.repository.GoldRepository;
import com.seop.treasurehuntgame.repository.HaveTreasureRepository;
import com.seop.treasurehuntgame.repository.TreasureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ChooseTreasureService {
    private final GoldRepository goldRepository;
    private final TreasureRepository treasureRepository;
    private final HaveTreasureRepository haveTreasureRepository;

    long choosePay = 0L;

    private void init(boolean isGeneral) {
        this.choosePay = isGeneral ? 3000L : 8000L; //일반뽑기 3000원 고급뽑기 8000원
    }

    public ChooseTreasureResponse getResult(boolean isGeneral) {
        this.init(isGeneral); //초기화시킨다
        boolean isEnoughGold = isStartChooseByGoldCheck(isGeneral); // 돈이충분한지 화인
        if (!isEnoughGold) throw new CNonGoldException(); // 충분하지않으면 빠구시킨다

        Gold gold = goldRepository.findById(1L).orElseThrow(CMissingDataException::new); // 1번 돈데이터 가져오기

        gold.putMinusGold(this.choosePay);

        goldRepository.save(gold);

        long treasureId = this.getTreasureId(isGeneral); // 랜덤으로 뽑은 보물 id 뽑아오기

        boolean isNewTreasure = false; // 새로 뽑은 보물을 체크하기위해 만든 변수 기본으로 false로 둠

        Optional<HaveTreasure> haveTreasure = haveTreasureRepository.findByTreasure_Id(treasureId); // 뽑힌 보물 아이디가 일치하는 보물의 정보를 가져옴
        if (haveTreasure.isEmpty()) throw new CMissingDataException(); // 만약 뽑힌 보물의 정보가없으면 에러를 보냄
        if (haveTreasure.get().getHaveCount() == 0) isNewTreasure = true; // 만약 원본데이터가 가지고있는 보유수량이 0 개면 새로뽑은거기때문에 ture로 변경
        haveTreasure.get().putCountPlus(); // Optional이기때문에 .get필요  +1 함

        haveTreasureRepository.save(haveTreasure.get()); // +1한걸 저장

        ChooseTreasureResponse result = new ChooseTreasureResponse();
        result.setAddGoldResponse(new AddGoldResponse.AddGoldResponseBuilder(gold).build());
        result.setChooseTreasureCurrentItem(new ChooseTreasureCurrentItem.ChooseTreasureCurrentItemBuilder(haveTreasure.get().getTreasure(), isNewTreasure).build());
        result.setUseTreasureItem(this.getMyTreasure());

        return result;
    }

    /**
     * 내가 보유한 보물들을 불러온다
     * @return 보유한 보물 컬렉션
     */
    public List<UseTreasureItem> getMyTreasure() {
        List<HaveTreasure> originList = haveTreasureRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L);

        List<UseTreasureItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new UseTreasureItem.UseTreasureItemBuilder(item).build()));

        return result;
    }
    /**
     * 뽑기를 진행함에 있어서 돈이 충분한지 확인하는 메서드
     * @param isGeneral 일반뽑기 여부
     * @return true는 충분하다 false는 불충분하다.
     */
    private boolean isStartChooseByGoldCheck(boolean isGeneral) {
        Gold gold = goldRepository.findById(1L).orElseThrow(CMissingDataException::new); // 1번 돈데이터 가져오기

        boolean result = false; // 기본으로 일단 막아두기

        if (gold.getHaveGold() >= choosePay) result = true; //내가가진 비용이 뽑기비용보다 많으면

        return result;
    }
    /**
     * 랜덤으로 보물 하나 뽑아오기
     * @param isGeneral 일반뽑기 여부
     * @return 랜덤으로뽑인 보물 시퀀르
     */
    private long getTreasureId(boolean isGeneral) {
        List<Treasure> originList = treasureRepository.findAll();

        List<ChooseTreasureItem> result = new LinkedList<>();

        double minPercent = 0D; //id = 0 번에 최소값이 0이기 때문
        for (Treasure item : originList) {
            ChooseTreasureItem addItem = new ChooseTreasureItem(); // 리스트에 담을 그릇

            addItem.setId(item.getId());        // 아이디를 가져옴
            addItem.setMinPercent(minPercent);  // 최소값은 minPercent이며  += item.getGeneralProbability() 할에정
            if (isGeneral) {
                addItem.setMaxPercent(minPercent + item.getGeneralProbability()); // 최대값은 최소값 + item.getGeneralProbability()
            } else {
                addItem.setMaxPercent(minPercent + item.getHighQualityProbability()); // 최대값은 최소값 + item.getHighQualityProbability()
            }

            result.add(addItem); // result에 addItemd을 넣어준다

            if (isGeneral) {
                minPercent += item.getGeneralProbability(); // 최소값은 최소값 + GeneralProbability()이기때문에
            } else {
                minPercent += item.getHighQualityProbability(); // 최소값은 최소값 + HighQualityProbability()이기때문에
            }
        }

        double percentResult = Math.random() * 100; //최대 100까지 랜덤숫자가 나온다

        long resultId = 0; //조건에 해당하는 id를 알아내기위해 기본값을 0으로 둠

        for (ChooseTreasureItem item : result) { //for문을 통해 result를 하나씩 던져 주면서
            // 만약 랜덤으로 생성된 숫자가 아이템에 최소값보다 크거나 같거나 랜덤으로 생성된 숫자가 아이템 최대값보다 작으면
            // resultId에다가 조건문에 해당되는 아이템 아이디를 넣는다
            // 포문을 다도는것이 아니라 만약 랜덤으로 생성되는 숫자가 일치하면 멈추기 위해 break; 를 사용
            if (percentResult >= item.getMinPercent() && percentResult <= item.getMaxPercent()) {
                resultId = item.getId();
                break;
            }
        }
        return resultId;
    }
}

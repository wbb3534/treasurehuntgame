package com.seop.treasurehuntgame.model;

import com.seop.treasurehuntgame.entity.Treasure;
import com.seop.treasurehuntgame.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ChooseTreasureCurrentItem {
    @ApiModelProperty(value = "보물 이미지")
    private String treasureImage;
    @ApiModelProperty(value = "보물 이름")
    private String treasureName;
    @ApiModelProperty(value = "보물 등급")
    private String treasureGrade;
    @ApiModelProperty(value = "보물 등급 이름(한글명)")
    private String treasureGradeName;
    @ApiModelProperty(value = "새로운 유물인지 아닌지")
    private Boolean isNew;

    private ChooseTreasureCurrentItem(ChooseTreasureCurrentItemBuilder builder) {
        this.treasureImage = builder.treasureImage;
        this.treasureName = builder.treasureName;
        this.treasureGrade = builder.treasureGrade;
        this.treasureGradeName = builder.treasureGradeName;
        this.isNew = builder.isNew;
    }
    public static class ChooseTreasureCurrentItemBuilder implements CommonModelBuilder<ChooseTreasureCurrentItem> {
        private final String treasureImage;
        private final String treasureName;
        private final String treasureGrade;
        private final String treasureGradeName;
        private final Boolean isNew;

        public ChooseTreasureCurrentItemBuilder(Treasure treasure, boolean isNew) {
            this.treasureImage = treasure.getTreasureImage();
            this.treasureName = treasure.getTreasureName();
            this.treasureGrade = treasure.getTreasureGrade().toString();
            this.treasureGradeName = treasure.getTreasureGrade().getName();
            this.isNew = isNew;
        }
        @Override
        public ChooseTreasureCurrentItem build() {
            return new ChooseTreasureCurrentItem(this);
        }
    }
}

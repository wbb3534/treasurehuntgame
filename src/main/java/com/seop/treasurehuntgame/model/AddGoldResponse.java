package com.seop.treasurehuntgame.model;

import com.seop.treasurehuntgame.entity.Gold;
import com.seop.treasurehuntgame.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AddGoldResponse {
    @ApiModelProperty(value = "보유골드량")
    private Long haveGold;

    private AddGoldResponse(AddGoldResponseBuilder builder) {
        this.haveGold = builder.haveGold;
    }
    public static class AddGoldResponseBuilder implements CommonModelBuilder<AddGoldResponse> {
        private final Long haveGold;

        public AddGoldResponseBuilder(Gold gold) {
            this.haveGold = gold.getHaveGold();
        }

        @Override
        public AddGoldResponse build() {
            return new AddGoldResponse(this);
        }
    }
}

package com.seop.treasurehuntgame.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChooseTreasureItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "최소 퍼센트")
    private Double minPercent;
    @ApiModelProperty(value = "최대 퍼센트")
    private Double maxPercent;
}

package com.seop.treasurehuntgame.model;

import com.seop.treasurehuntgame.entity.HaveTreasure;
import com.seop.treasurehuntgame.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UseTreasureItem {
    @ApiModelProperty(value = "보물 이미지")
    private String treasureImage;
    @ApiModelProperty(value = "보물 이름")
    private String treasureName;
    @ApiModelProperty(value = "보물 등급")
    private String treasureGrade;
    @ApiModelProperty(value = "보물 등급 이름(한글명)")
    private String treasureGradeName;
    @ApiModelProperty(value = "보물 보유수량")
    private Integer treasureCount;

    private UseTreasureItem(UseTreasureItemBuilder builder) {
        this.treasureImage = builder.treasureImage;
        this.treasureName = builder.treasureName;
        this.treasureGrade = builder.treasureGrade;
        this.treasureGradeName = builder.treasureGradeName;
        this.treasureCount = builder.treasureCount;
    }
    public static class UseTreasureItemBuilder implements CommonModelBuilder<UseTreasureItem> {
        private final String treasureImage;
        private final String treasureName;
        private final String treasureGrade;
        private final String treasureGradeName;
        private final Integer treasureCount;
        public UseTreasureItemBuilder(HaveTreasure haveTreasure) {
            this.treasureImage = haveTreasure.getTreasure().getTreasureImage();
            this.treasureName = haveTreasure.getTreasure().getTreasureName();
            this.treasureGrade = haveTreasure.getTreasure().getTreasureGrade().toString();
            this.treasureGradeName = haveTreasure.getTreasure().getTreasureGrade().getName();
            this.treasureCount = haveTreasure.getHaveCount();
        }
        @Override
        public UseTreasureItem build() {
            return new UseTreasureItem(this);
        }
    }
}

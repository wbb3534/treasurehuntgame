package com.seop.treasurehuntgame.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GameDataResponse {
    @ApiModelProperty(value = "보유골드량")
    private AddGoldResponse addGoldResponse;
    @ApiModelProperty(value = "나의 유물 정보 리스트")
    private List<UseTreasureItem> useTreasureItem;
}

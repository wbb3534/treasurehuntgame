package com.seop.treasurehuntgame.model;

import com.seop.treasurehuntgame.enums.TreasureGrade;
import com.seop.treasurehuntgame.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TreasureRequest {
    @ApiModelProperty(value = "보물 등급")
    @NotNull
    @Length(min = 1, max = 10)
    @Enumerated(value = EnumType.STRING)
    private TreasureGrade treasureGrade;
    @ApiModelProperty(value = "보물 이름")
    @NotNull
    @Length(min = 1, max = 30)
    private String treasureName;
    @ApiModelProperty(value = "보물 이미지")
    @NotNull
    @Length(min = 1, max = 30)
    private String treasureImage;
    @ApiModelProperty(value = "일반상자 확률")
    @NotNull
    private Double generalProbability;
    @ApiModelProperty(value = "고급상자 확률")
    @NotNull
    private Double highQualityProbability;

    private TreasureRequest(TreasureRequestBuilder builder) {
        this.treasureGrade = builder.treasureGrade;
        this.treasureName = builder.treasureName;
        this.treasureImage = builder.treasureImage;
        this.generalProbability = builder.generalProbability;
        this.highQualityProbability = builder.highQualityProbability;
    }
    public static class TreasureRequestBuilder implements CommonModelBuilder<TreasureRequest> {
        private final TreasureGrade treasureGrade;
        private final String treasureName;
        private final String treasureImage;
        private final Double generalProbability;
        private final Double highQualityProbability;

        public TreasureRequestBuilder(TreasureGrade treasureGrade,
                                      String treasureName,
                                      String treasureImage,
                                      Double generalProbability,
                                      Double highQualityProbability) {
            this.treasureGrade = treasureGrade;
            this.treasureName = treasureName;
            this.treasureImage = treasureImage;
            this.generalProbability = generalProbability;
            this.highQualityProbability = highQualityProbability;

        }
        @Override
        public TreasureRequest build() {
            return new TreasureRequest(this);
        }
    }
}

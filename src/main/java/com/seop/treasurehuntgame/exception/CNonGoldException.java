package com.seop.treasurehuntgame.exception;

public class CNonGoldException extends RuntimeException {
    public CNonGoldException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNonGoldException(String msg) {
        super(msg);
    }

    public CNonGoldException() {
        super();
    }
}

package com.seop.treasurehuntgame.entity;

import com.seop.treasurehuntgame.enums.TreasureGrade;
import com.seop.treasurehuntgame.interfaces.CommonModelBuilder;
import com.seop.treasurehuntgame.model.TreasureRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Treasure {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "보물 등급")
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private TreasureGrade treasureGrade;
    @ApiModelProperty(value = "보물 이름")
    @Column(nullable = false, length = 30)
    private String treasureName;
    @ApiModelProperty(value = "보물 이미지")
    @Column(nullable = false, length = 30)
    private String treasureImage;
    @ApiModelProperty(value = "일반상자 확률")
    @Column(nullable = false)
    private Double generalProbability;
    @ApiModelProperty(value = "고급상자 확률")
    @Column(nullable = false)
    private Double highQualityProbability;

    private Treasure(TreasureBuilder builder) {
        this.treasureGrade = builder.treasureGrade;
        this.treasureName = builder.treasureName;
        this.treasureImage = builder.treasureImage;
        this.generalProbability = builder.generalProbability;
        this.highQualityProbability = builder.highQualityProbability;
    }
    public static class TreasureBuilder implements CommonModelBuilder<Treasure> {
        private final TreasureGrade treasureGrade;
        private final String treasureName;
        private final String treasureImage;
        private final Double generalProbability;
        private final Double highQualityProbability;

        public TreasureBuilder(TreasureRequest request) {
            this.treasureGrade = request.getTreasureGrade();
            this.treasureName = request.getTreasureName();
            this.treasureImage = request.getTreasureImage();
            this.generalProbability = request.getGeneralProbability();
            this.highQualityProbability = request.getHighQualityProbability();
        }

        @Override
        public Treasure build() {
            return new Treasure(this);
        }
    }

}

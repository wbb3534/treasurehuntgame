package com.seop.treasurehuntgame.entity;

import com.seop.treasurehuntgame.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HaveTreasure {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "보물 시퀀스")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "treasureId", nullable = false)
    private Treasure treasure;
    @ApiModelProperty(value = "보유 수량")
    @Column(nullable = false)
    private Integer haveCount;
    public void resetCount() {
        haveCount = 0;
    }
    public void putCountPlus() {
        haveCount += 1;
    }
    private HaveTreasure(HaveTreasureBuilder builder) {
        this.treasure = builder.treasure;
        this.haveCount = builder.haveCount;
    }
    public static class HaveTreasureBuilder implements CommonModelBuilder<HaveTreasure> {
        private final Treasure treasure;
        private final Integer haveCount;

        public HaveTreasureBuilder(Treasure treasure) {
            this.treasure = treasure;
            this.haveCount = 0;
        }
        @Override
        public HaveTreasure build() {
            return new HaveTreasure(this);
        }
    }

}

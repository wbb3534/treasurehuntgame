package com.seop.treasurehuntgame.entity;

import com.seop.treasurehuntgame.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Gold {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "보유골드량")
    @Column(nullable = false)
    private Long haveGold;
    public void resetGold() {
        this.haveGold = 0L;
    }
    public void putAddGold() {
        this.haveGold += 300;
    }
    public void putMinusGold(long gold) {
        this.haveGold -= gold;
    }
    private Gold(GoldBuilder builder) {
        this.haveGold = builder.haveGold;
    }
    public static class GoldBuilder implements CommonModelBuilder<Gold> {
        private final Long haveGold;

        public GoldBuilder() {
            this.haveGold = 0L; //0이 Long타입입을 표시하기 위해 L
        }
        @Override
        public Gold build() {
            return new Gold(this);
        }
    }
}
